package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;

public class IncrementTest {
 
    private Increment increment;
 
    @Before
    public void setUp() {
        increment = new Increment();
    }
 
    @Test
    public void testGetCounter() {
        assertEquals(1, increment.getCounter());
        assertEquals(2, increment.getCounter());
    }
 
    @Test
    public void testDecreaseCounter() {
        assertEquals(1, increment.decreasecounter(0));
        assertEquals(0, increment.decreasecounter(1));
        assertEquals(0, increment.decreasecounter(2));
    }
}
